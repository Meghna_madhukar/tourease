package com.te.tourease;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CustomerDao;
import com.model.Customer;

import net.bytebuddy.utility.RandomString;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class CustomerController {
	
	@Autowired
	CustomerDao customerDao;
	
	@PostMapping("addCustomer")
	public Customer addCustomer(@RequestBody Customer customer){
		String randomCode = RandomString.make(6);
		customer.setVerificationCode(randomCode);
		customerDao.sendEmail(customer.getEmailId(), "Dear "+customer.getFirstName()+", Your Email Verification OTP is: "+customer.getVerificationCode()+".");
		return customerDao.addCustomer(customer);
	}
	
	@GetMapping("customerLogin/{emailId}/{password}")
	public Customer employeeLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password){
		System.out.println(customerDao.customerLogin(emailId, password));
		return customerDao.customerLogin(emailId, password);
	}
	
	
}
