package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Customer;

@Service
public class CustomerDao {
	@Autowired
	CustomerRepository customerRepo;
	
	@Autowired
	private JavaMailSender mailSender;

	
	//Password Encryption and generating OTP
	public Customer addCustomer(Customer customer){
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		String encryptedPassword = bcrypt.encode(customer.getPassword());
		customer.setPassword(encryptedPassword);
		return customerRepo.save(customer);
	}
	
	//Fetching details by converting password to encrypted password
	public Customer customerLogin(String emailId, String password) {
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		String encodedPassword = customerRepo.findByEmail(emailId).getPassword();
		if(bcrypt.matches(password, encodedPassword)){
			return customerRepo.findByEmail(emailId);
		}
		return null;
	}
	
	public void sendEmail(String toEmail, String body){
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("gundetimeghna1009@gmail.com");
		message.setTo(toEmail);
		message.setSubject("Email Verification");
		message.setText(body);
		mailSender.send(message);
		System.out.println("Mail sent successfully");
	}
}
