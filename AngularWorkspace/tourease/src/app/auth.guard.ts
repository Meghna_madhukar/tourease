import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { TourService } from './tour.service';

@Injectable()
export class authGuard implements CanActivate {
  constructor(private tourService: TourService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.tourService.getLoginStatus() && this.tourService.isAuthenticated()) {
      return true;
    } else {
      // Redirect to login or another page if not logged in
      this.router.navigate(['/login']);
      return false;
    }
  }
}
