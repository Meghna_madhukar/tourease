import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgToastService } from 'ng-angular-popup';
import { Router } from '@angular/router';
import { TourService } from '../tour.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css'],
})
export class ForgotpasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  currentStep: 'sendOtp' | 'verifyOtp' | 'updatePassword' = 'sendOtp';
  countries: any;

  constructor(
    private fb: FormBuilder,
    private service: TourService,
    private toast: NgToastService,
    private router: Router
  ) {
    this.forgotPasswordForm = this.fb.group({
      countryCode: ['', [Validators.required]],
      mobileNumber: ['', [Validators.required]],
      otp: ['', [Validators.required]],
      newPassword: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required], // Add this line for the confirm password field
    }, { validators: this.passwordMatchValidator });

  }

  ngOnInit() {
    // Fetch country codes when the component initializes
    this.service.getAllCountriesCodes().subscribe((data: any) => {
      this.countries = data;
    });
  }
  sendOtp() {
    const countryCodeControl = this.forgotPasswordForm.get('countryCode');
    const mobileNumberControl = this.forgotPasswordForm.get('mobileNumber');

    if (countryCodeControl && mobileNumberControl) {
      const countryCode: string = countryCodeControl.value;
      const mobileNumber: string = mobileNumberControl.value;

      // Make sure countryCode includes the '+' sign
      const fullPhoneNumber = countryCode + mobileNumber;

      console.log(fullPhoneNumber);

      this.service.sendOtp(fullPhoneNumber).subscribe(
        (response) => {
          this.toast.success({
            detail: '',
            summary: response,
            duration: 2000,
            position: 'topCenter',
          });
          this.currentStep = 'verifyOtp';
        },
        (error) => {
          console.error('Error sending OTP:', error);
          this.handleError(error, 'Failed to send OTP');
        }
      );
    }
  }


  verifyOtp() {
    const countryCode = this.forgotPasswordForm.value.countryCode;
    const mobileNumber = this.forgotPasswordForm.value.mobileNumber;
    const otp = this.forgotPasswordForm.value.otp;

    const fullPhoneNumber = countryCode + mobileNumber;

    this.service.verifyOtp(fullPhoneNumber, otp).subscribe(
      () => {
        this.toast.success({
          detail: '',
          summary: 'OTP verified successfully',
          duration: 2000,
          position: 'topCenter',
        });
        this.currentStep = 'updatePassword';
      },
      (error) => {
        console.error('Error verifying OTP:', error);

        if (error.status === 401) {
          // Unauthorized access error
          this.handleError(error, 'Unauthorized access. Please verify your OTP and try again.');
        } else {
          this.handleError(error, 'Failed to verify OTP');
        }
      }
    );
  }


  updatePassword() {
    const countryCode = this.forgotPasswordForm.value.countryCode;
    const mobileNumber = this.forgotPasswordForm.value.mobileNumber;
    const newPassword = this.forgotPasswordForm.value.newPassword;

    const fullPhoneNumber = countryCode + mobileNumber;
    const confirmPassword = this.forgotPasswordForm.value.confirmPassword;
    if (newPassword !== confirmPassword) {
      this.toast.error({
        detail: '',
        summary: 'Passwords do not match',
        duration: 2000,
        position: 'topCenter',
      });
      return;
    }

    this.service.updatePassword(fullPhoneNumber, newPassword).subscribe(
      () => {
        this.toast.success({
          detail: '',
          summary: 'Password updated successfully. Login now!!!',
          duration: 2000,
          position: 'topCenter',
        });
        this.router.navigate(['/login']);
      },
      (error) => {
        console.error('Error updating password:', error);
        this.handleError(error, 'Failed to update password');
      }
    );
  }
  private passwordMatchValidator(formGroup: FormGroup) {
    const newPasswordControl = formGroup.get('newPassword');
    const confirmPasswordControl = formGroup.get('confirmPassword');

    if (!newPasswordControl || !confirmPasswordControl) {
      return null;
    }

    if (newPasswordControl.value !== confirmPasswordControl.value) {
      confirmPasswordControl.setErrors({ passwordMismatch: true });
    } else {
      confirmPasswordControl.setErrors(null);
    }

    return null;
  }

  private handleError(error: any, defaultErrorMessage: string) {
    let errorMessage = defaultErrorMessage;

    if (error.status === 404) {
      errorMessage = 'User not found';
    } else if (error.status === 401) {
      errorMessage = 'Unauthorized access';
    } else if (error.error && error.error.message) {
      errorMessage = error.error.message;
    }

    this.toast.error({
      detail: '',
      summary: errorMessage,
      duration: 2000,
      position: 'topCenter',
    });
  }
}
