import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { NgToastModule } from 'ng-angular-popup';
import { NgxCaptchaModule } from 'ngx-captcha';
import { CustomerdetailsComponent } from './customerdetails/customerdetails.component';
import { PackagesComponent } from './packages/packages.component';
import { AboutComponent } from './about/about.component';
import { authGuard } from './auth.guard';
import { HomepageComponent } from './homepage/homepage.component';
<<<<<<< HEAD
import { FooterComponent } from './footer/footer.component';

=======
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
>>>>>>> b50582ab869c55d367b797da0ab46ed6d4edd544
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HeaderComponent,
    LoginComponent,
    CustomerdetailsComponent,
    PackagesComponent,
    AboutComponent,
    HomepageComponent,
<<<<<<< HEAD
    FooterComponent,
   
=======
    ForgotpasswordComponent
>>>>>>> b50582ab869c55d367b797da0ab46ed6d4edd544
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    NgToastModule,
    NgxCaptchaModule,
    ReactiveFormsModule,
  ],
  providers: [authGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
