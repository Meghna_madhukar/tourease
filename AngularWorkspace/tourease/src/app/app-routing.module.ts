import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { authGuard } from './auth.guard';
import { CustomerdetailsComponent } from './customerdetails/customerdetails.component';
import { PackagesComponent } from './packages/packages.component';
import { AboutComponent } from './about/about.component';
import { HomepageComponent } from './homepage/homepage.component';

const routes: Routes = [
  {path:'register',    component:RegisterComponent},
  {path:'login',    component:LoginComponent},
  {path:'customerdetails',canActivate:[authGuard], component:CustomerdetailsComponent},
  {path:'packages', canActivate:[authGuard], component:PackagesComponent},
<<<<<<< HEAD
  {path:'about',  component:AboutComponent},
  {path:'homepage', component:HomepageComponent}
=======
  {path:'about', canActivate:[authGuard], component:AboutComponent},
  {path:'homepage', component:HomepageComponent},
  {path:'packageinfo', component:PackageinfoComponent}
>>>>>>> b50582ab869c55d367b797da0ab46ed6d4edd544
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
