import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-packages',
    templateUrl: './packages.component.html',
    styleUrl: './packages.component.css'
 })
export class PackagesComponent implements OnInit {
  
  packages: any;
  cartProducts: any;

  constructor() {

    this.cartProducts = [];

    this.packages = [
      {id:1001, name:"Hills & HouseBoat: Munnar & Alleppey Package", price:21280.00, primarydestination:"Munnar, Alleppey",secondarydestination:"Munnar Spice Plantations, waterfalls, Eravikulam National Park, Mattupetty Dam, Periyar Wildlife Sanctury, Allepy Houseboat Stay, Backwaters, St.Francis Church", duration:"5 Nights/6 Days", inclusions:"Upto 3 stars,Meals,Sightseeing,Transfers,Honeymoon Freebies", imgsrc:"assets/Images/Kerala.jpg"},
      {id:1002, name:"Best Selling Goa Trip Package For Friends", price:9999.00, primarydestination:"Goa", secondarydestination:"Miramar Beach, Panjim Market, Dona Paula Bay, Basilica Bon of Jesus & Se Cathedral", duration:"3 Nights/4 Days", inclusions: "Upto 3 stars ,Meals, Sightseeing, Stay Included, Transfers", imgsrc:"assets/Images/Goa.jpg"},
      {id:1003, name:"Trendy Thailand Tour Package", price:35000.00, primarydestination:"Thailand", secondarydestination:"Buddha Temple, lad Koh, Viewpoint, Koh samui, Wat Traimit and Wat Pho in Bangkok, Ang Thong National, Marine Park, Grand Palace, City Pillar Shrine", duration:"5 Nights/6 Days", inclusions:"Upto 3 stars, Flights, Meals, Sightseeing, Transfers", imgsrc:"assets/Images/thailand.jpg"},
      {id:1004, name:"Europe Tour Package in Summer", price:136500.00, primarydestination:"Paris", secondarydestination:"Eiffel Tower, Montparnasse Tower, Mount Rigi, Mount Titlis, Gondola Ride, Zurich", duration:"5 Nights/6 Days", inclusions:"Upto 3 stars, Flights, Meals, Sightseeing, Transfers", imgsrc:"assets/Images/paris.jpg"},
      {id:1005, name:"Delightful Dubai Tour", price:25000.00, primarydestination:"Dubai", secondarydestination:"Dhow Cruise, Dubai Creek, Burj Khalifa, Jumeirah Beach, Palm Island, Dune Bashing", duration:"4 Nights/5 Days", inclusions:"Upto 4 stars, Meals, Sightseeing, Entry Tickets, Airport Transfers, Visa", imgsrc:"assets/Images/dubai.jpg"},
      {id:1006, name:"The Luxury Tales From Bali", price:40000.00, primarydestination:"Bali", secondarydestination:"Nasa Dua Water Sports, Turtle Island, Dinner Cruise in Seminyak, Barong kintamani", duration:"6 Nights/7 Days", inclusions:"Upto 5 stars, Meals, Sightseeing, Stay Included, Transfers", imgsrc:"assets/Images/bali.webp"},
      {id:1007, name:"Exotic Srilanka sightseeing Tour Package", price:26000.00, primarydestination:"Srilanka", secondarydestination:"Kandy Lake, Pinnawala, Elephant Orphanage, Gregory Lake, Strawberry Gardens, Hakgala Botanical Garden & Nuwara Eliya Gardens, Bentota, Madhura River, Turtle Hatchery, Colombo, Galle Face Green, Viharamahadevi Park", duration:"4 Nights/5 Days", inclusions:"Upto 4 stars, Meals, Sightseeing, Stay Included, Transfers", imgsrc:"assets/Images/srilanka.jpg"},
      {id:1008, name:"Experience The Thrill of LEGOLAND and Universal Studio", price:49000.00, primarydestination:"Singapore", secondarydestination:"Sentosa, Cable Car Ride, Universal Studios, LEGOLAND Theme Park, S.E.A.Aquarium, Wings Of Time Show", duration:"4 Nights/5 Days", inclusions:"Upto 4 stars, Meals, Sightseeing, Transfers", imgsrc:"assets/Images/singapore.jpg"},
      {id:1009, name:"Soulful Darjeeling Gangtok Sikkim Honeymoon Package", price:39414.00, primarydestination:"Sikkim", secondarydestination:"Gangtok, Do Drul Chorten, Namgyal Institute of Tibetology, Tashi Viewport, Ganesh Tok, Hanuman Tok, Darjeeling, Mt.Kanchendzonga, Ghoom Monastery, Himalayan Mounteering Institute, Tibetan Refugee Centre", duration:"4 Nights/5 Days", inclusions:"Upto 2 stars, Meals, Sightseeing, Airport Pickup-Drop", imgsrc:"assets/Images/sikkim.jpg"},
    ];
   
  }

  ngOnInit() {    
  }

  addToCart(product: any) {
    this.cartProducts.push(product);
    localStorage.setItem("cartItems", JSON.stringify(this.cartProducts));
  }
}
