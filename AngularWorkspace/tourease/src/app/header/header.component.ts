import { Component, OnInit } from '@angular/core';
import { TourService } from '../tour.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: boolean = false;

  constructor(private authService: TourService) {
   
  }

  ngOnInit() {
    this.authService.getIsUserLogged().subscribe((isLogged: boolean) => {
      this.isLoggedIn = isLogged;
    });
  }

  isAuthenticated(): boolean {
    return this.authService.isAuthenticated();
  }

  logout() {
    this.authService.logout();
  }
}
