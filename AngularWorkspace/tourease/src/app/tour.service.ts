import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TourService {

  loginStatus: boolean;
  isUserLogged: Subject<boolean> = new Subject<boolean>();

  constructor(private http: HttpClient) { 
    this.loginStatus = false;
  }

  setLoginStatus() {
    this.loginStatus = true;
    this.isUserLogged.next(true);
  }

  getLoginStatus(): boolean {
    return this.loginStatus;
  }

  getIsUserLogged(): Subject<boolean> {
    return this.isUserLogged;
  }

  setLogoutStatus() {
    this.loginStatus = false;
    this.isUserLogged.next(false);
  }

  login() {
    this.setLoginStatus();
  }

  logout() {
    this.setLogoutStatus();
  }

  isAuthenticated(): boolean {
    return this.loginStatus;
  }

  customerLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8083/customerLogin/' + emailId + '/' + password).toPromise();
  }

  sendOtp(mobileNumber: any): Observable<any> {
    return this.http.post('http://localhost:8085/forgot-password/send-otp', { mobileNumber }, { responseType: 'text' });
  }

  verifyOtp(mobileNumber: string, otp: any): Observable<any> {
    return this.http.post('http://localhost:8085/forgot-password/verify-otp', { mobileNumber, otp }, { headers: { 'Content-Type': 'application/json' } });
  }

  updatePassword(mobileNumber: string, newPassword: string): Observable<any> {
    const data = { mobileNumber, newPassword };
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post('http://localhost:8085/forgot-password/reset-password', data, { headers });
  }

  getAllCountriesCodes():any{
    return this.http.get("https://restcountries.com/v3.1/all");
  }

}
