import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private baseUrl = 'https://www.universal-tutorial.com/api/';
  private apiToken = 'jPUhYd5O5czzH81JtEGFeaep8U3X9iyiyeZq21QEbspLuBDkGMIrJHLXV-uBYqdJmps';
  private userEmail = 'meghanamadhukar@gmail.com';
  private authToken: string;

  constructor(private http: HttpClient) {
    this.authToken = "";
  }

  getAccessToken(): Observable<any> {
    const headers = new HttpHeaders({
      'Accept': 'application/json',
      'api-token': this.apiToken,
      'user-email': this.userEmail,
    });

    return this.http.get(`${this.baseUrl}getaccesstoken`, { headers });
  }

  getCountries(): Observable<any[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.authToken}`,
      'Accept': 'application/json',
    });

    return this.http.get<any[]>(`${this.baseUrl}countries`, { headers });
  }

  getStates(country: any): Observable<any[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.authToken}`,
      'Accept': 'application/json',
    });
    console.log(country);
    return this.http.get<any[]>(`${this.baseUrl}states/${country}`, { headers });
  }

  getCities(state: string): Observable<any[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.authToken}`,
      'Accept': 'application/json',
    });

    return this.http.get<any[]>(`${this.baseUrl}cities/${state}`, { headers });
  }

  setAuthToken(token: string): void {
    this.authToken = token;
  }

  registerCustomer(customer: any): any {
    return this.http.post('http://localhost:8083/addCustomer', customer);
  }
<<<<<<< HEAD
  

=======

  addtourPackage(tourpackage: any): any{
    return this.http.post('http://localhost:8083/addtourPackage',tourpackage);
  }
>>>>>>> b50582ab869c55d367b797da0ab46ed6d4edd544
}
