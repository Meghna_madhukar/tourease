import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Subject, debounceTime } from 'rxjs';
import { Router } from '@angular/router';
import { NgToastComponent } from 'ng-angular-popup';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  regForm:any;
  confirmPasswordMatch: boolean = true;
  confirmPasswordDirty: boolean = false;
  selectedState: any;
  selectedCountry: any;
  password: any;
  confirmPassword: any;
  countries: any[];
  states:any[];
  cities:any[];
  customer: any;

  private confirmPasswordChanged = new Subject<void>();

  constructor(private service: CustomerService, private router: Router, private toast: NgToastComponent){
   this.countries = [];
   this.states = [];
   this.cities = [];
   this.customer = {
    firstName: '',
    lastName:'',
    dob:'',
    gender:'',
    country:'',
    state:'',
    city:'',
    mobileNumber:'',
    emailId:'',
    password:'',
   }
  }
  ngOnInit(){
    this.service.getAccessToken().subscribe((response) => {
      const accessToken = response.auth_token;
      this.service.setAuthToken(accessToken);
      this.service.getCountries().subscribe((countriesList) => {
        this.countries = countriesList;
        console.log(countriesList);
      })
    })
    this.confirmPasswordChanged.pipe(debounceTime(1000)).subscribe(() => {
      this.confirmPasswordMatch = this.regForm.value.password === this.regForm.value.confirmPassword;
    });
  }
onCountrySelect(){
  if(this.selectedCountry != null) {
    this.service.getStates(this.selectedCountry).subscribe((statesList)=>{
      console.log(this.selectedCountry);
      this.states = statesList;
      console.log(statesList);
    });
  }
}
onStateSelect(){
  this.service.getCities(this.selectedState).subscribe((citiesList)=>{
    this.cities=citiesList;
    console.log(citiesList);
  })
}
registerSubmit(regForm: any) {
  console.log(regForm);
  this.customer.firstName = regForm.firstName;
  this.customer.lastName = regForm.lastName;
  this.customer.dob = regForm.dob;
  this.customer.gender = regForm.gender;
  this.customer.country = regForm.country;
  this.customer.state = regForm.state;
  this.customer.city = regForm.city;
  this.customer.mobileNumber = regForm.mobileNumber;
  this.customer.emailId = regForm.emailId;
  this.customer.password = regForm.password;  
  console.log(this.customer);
  this.service.registerCustomer(this.customer).subscribe((data: any) => {
    console.log(data);
    this.router.navigate(['login']);
  });
}
validateConfirmPassword() {
  this.confirmPasswordDirty = true;
  this.confirmPasswordMatch = this.regForm.value.password === this.regForm.value.confirmPassword;
}
}
