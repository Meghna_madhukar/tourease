import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { TourService } from '../tour.service';
import { NgToastService } from 'ng-angular-popup';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  customer: any;
  siteKey: string;

  constructor(
    private router: Router,
    private service: TourService,
    private toast: NgToastService,
    private ngZone: NgZone
  ) {
    this.siteKey = '6Ld-dUEpAAAAAPPZenwaim_U_JK7_OcKEjJ76OuY';
  }

  ngOnInit() {}


  onLogin(loginForm: NgForm){
    console.log(loginForm.value);
  }
  async loginSubmit(loginForm: any) {
    this.customer = null;
    localStorage.setItem('emailId', loginForm.emailId);

    if (loginForm.emailId === "Admin" && loginForm.password === "Admin") {
      this.service.setLoginStatus();
      this.router.navigate(['customerdetails'])
      this.toast.success({ detail: "", summary: "Login successful", duration: 5000, sticky: true, position: 'topRight' })

    } else {
      await this.service.customerLogin(loginForm.emailId, loginForm.password).then((data: any) => {
        this.customer = data;
      });
      if (this.customer != null) {
        this.service.setLoginStatus();
        this.toast.success({ detail: 'Success Message', summary: 'Login in successful', duration: 5000, sticky: true, position: 'topRight' });
        
        console.log('Before navigation to customerdetails');
        this.ngZone.run(() => {
          this.router.navigate(['/customerdetails']);
        });
        console.log('After navigation to customerdetails');
      } else {
        await this.service.customerLogin(loginForm.emailId, loginForm.password).then((data: any) => {
          this.customer = data;
        });

        if (this.customer != null) {
          this.service.setLoginStatus();
          this.ngZone.run(() => {
            this.router.navigate(['/packages']);
          });
        }

        this.toast.error({ detail: 'Error Message', summary: 'Invalid Credentials', duration: 5000, sticky: true, position: 'topRight' });
      }
      this.toast.error({ detail: "", summary: "Invalid Credentials", duration: 5000, sticky: true, position: 'topRight' })
      console.log(this.toast.error);
    }
  }
}

