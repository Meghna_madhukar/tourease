import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-packageinfo',
  templateUrl: './packageinfo.component.html',
  styleUrl: './packageinfo.component.css'
})
export class PackageinfoComponent implements OnInit {
  packageName: any;
  description: any;
  duration: any;
  departure: any;
  returndate: any;
  image: any;
  ngOnInit(): void { }
  submitPackageInfo(): void {
    // Handle the form submission logic here
    const packageInfo = {
      packageName: this.packageName,
      description: this.description,
      duration: this.duration,
      departure: this.departure,
      returnDate: this.returndate,
      image: this.image
    };

    console.log(packageInfo);
  }
  onFileChange(event: any): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      // Handle the file as needed (e.g., store it in a variable or perform other actions)
      this.image = file;
    }
  }
}
